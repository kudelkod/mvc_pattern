<?php

include_once (ROOT.'/models/News.php');

class NewsController{

    public function actionIndex(){
        $newsList = array();
        $newsList = News::getNewsList();

        print_r($newsList);
    }

    public function actionView($id){
        $newsItem = News::getNewsItemById($id);

        print_r($newsItem);
    }
}